var flat = {
	make: "Flat",
	model: "500",
	year: 1957,
	color: "Medium Blue",
	passengers: 2,
	convertible: false,
	milleage: 88000,
	started: false,
	fuel: 0,
	
	start: function() {
		if (this.fuel == 0 ) {
			alert("The car is on empty, fill up before starting!");
		} else {
			this.started = true;
		}
	},
	
	stop: function() {
		this.started = false;
	},
	
	drive: function() {
		if (this.started) {
			if (this.fuel > 0) {
				alert(this.make + " " + this.model + " goes zoom zoom!");
				this.fuel = this.fuel - 1;
			} else {
				alert("Uh oh, out of fuel.");
				this.stop();
			}
		} else {
			alert("You need to start the engine first.");
		}
	},
	
	addFuel: function(amount) {
		this.fuel = this.fuel + amount;
	}
	
}


var cadi = {
	make: "GM",
	model: "Cadillac",
	year: 1955,
	color: "tan",
	passengers: 5,
	convertible: false,
	mileage: 12892,
	started: false,
	fuel: 0,
	
	start: function() {
		this.started = true;
	},
	
	stop: function() {
		this.started = false;
	},
	
	drive: function() {
		if (this.started) {
			if (this.fuel > 0) {
				alert(this.make + " " + this.model + " goes zoom zoom!");
				this.fuel = this.fuel - 1;
			} else {
				alert("Uh oh, out of fuel.");
				this.stop();
			}
		} else {
			alert("You need to start the engine first.");
		}
	},
	
	addFuel: function(amount) {
		this.fuel = this.fuel + amount;
	}
}

var chevy = {
	make: "Chevy",
	model: "Bel Air",
	year: 1957,
	color: "red",
	passengers: 2,
	convertible: false,
	mileage: 1021
};